let arreglo = [3,45,99, 02, 3];

for(i=0; i<arreglo.length; i++){
    console.log(arreglo[i]);
}

console.log("** forEach es mas legible, para cada elemento se ejecuta una funciona enviado como argumento el elemento navegado")
arreglo = ["ccs", "html", "js", "python"];
arreglo.forEach(function(item){
    console.log(item);
});

console.log("** Arreglo filtrado");
arreglo = arreglo.filter(function(item){
    return item != 'ruby';
});
arreglo.forEach(function(item){
    console.log(item);
});

console.log("** Arreglo filtrado con Arrow Function");
arreglo = arreglo.filter((item)=> item != 'js');
arreglo.forEach(function(item){
    console.log(item);
});

console.log("Encontrar elemento en el arreglo");
let el = arreglo.find((el)=> el == "html");
console.log(el);

console.log("Map permite ejecutar operaciones sobre los elementos");
arreglo = [3,45,99, 02, 3];
let cuadrados = arreglo.map((item)=>item*item);
console.log(cuadrados);