//se define la clase constructora desde la cual se puede instanciar

class Curso{
    constructor(titulo, duracion){
        this.titulo = titulo;
        this.duracion = duracion;
        this.color = "yellow";
    }
}

//Herencia: permite reutilizar codigo, tambien se pueden sobre-escribir metodos y propiedades
class User{
    constructor(nombre){
        this.nombre = nombre;
    }

    saludar(){
        console.log("Hola " + this.nombre);
    }
}

class Admin extends User{
    constructor(nombre){
        super(nombre);
    }

    saludar(){
        super.saludar();
        console.log("Panel de Administracion");
    }
}

let admin = new Admin("Charly");
admin.saludar();