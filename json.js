let curso = {
    titulo: "JS Profesional",
    duracion: 5.5,
    secciones: ["introduccion", "arreglos","etc"],

    inscribir: function(usuario) {console.log("Hola " + usuario);}
}

console.log(curso.titulo);;
console.log(curso["duracion"])

curso.inscribir("SK");

//Funcion Constructora: permite definir una funcion que genera la estructura del objeto
console.log("** Funcion constructora");

function Curso(titulo){
    this.titulo = titulo;
    this.inscribir = function(usuario){
        console.log(usuario + " esta inscrito");
    }
}

let cursoJS = new Curso("Curso JS");
let cursoCCS = new Curso("Master CCS");

console.log(cursoJS.titulo);
cursoJS.inscribir("Charly");
console.log(cursoCCS['titulo']);